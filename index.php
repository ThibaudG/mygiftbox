<?php
session_start();
include_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager;
use giftbox\Controleur\Controleur;
use giftbox\Controleur\ControleurCatalogue;
use conf\Eloquent;

\Slim\Slim::registerAutoloader();

Eloquent::init('src/conf/config.ini');

$app = new \Slim\slim();

// Accueil Controleur
$app->get('/accueil',function() use ($app){
    $control = new Controleur($app);
    $control->afficherAccueil();
})->name('accueil');

// Catalogue Controleur
$app->get('/catalogue/:categNum/:triPar',function($categNum,$triPar) use ($app){
    $control = new ControleurCatalogue($app);
    $control->afficherCatalogue($categNum,$triPar);
})->name('catalogue');

$app->get('/prestation/:id',function($id) use ($app){
    $control = new Controleur($app);
    $control->afficherPrestation($id);
})->name('prestation');

// Panier Controleur
$app->get('/panier',function() use ($app){
    $control = new Controleur($app);
    $control->afficherPanier();
})->name('panier');

$app->get('/recapitulatif',function() use ($app){
    $control = new Controleur($app);
    $control->afficherRecap();
})->name('recapitulatif');

$app->post('/payement',function() use ($app){
    $control = new Controleur($app);
    $control->afficherPaye();
})->name('payement');

$app->get('/inscription',function() use ($app){
    $control = new Controleur($app);
    $control->afficherInscription("");
})->name('inscription');

$app->post('/register',function() use ($app){
    $control = new Controleur($app);
    $control->inscription();
})->name('useradd');

$app->get('/connexion',function() use ($app){
    $control = new Controleur($app);
    $control->afficherConnexion("");
})->name('connexion');

$app->post('/signin',function() use ($app){
    $control = new Controleur($app);
    $control->connexion();
})->name('connexion_session');

$app->get('/deconnexion',function() use ($app){
    $control = new Controleur($app);
    $control->deconnexion();
})->name('deconnexion');

$app->get('/ajout/:id/:aff',function($id,$aff) use ($app){
    $control = new ControleurCatalogue($app);
    $control->ajouterPrestationCoffret($id,$aff);
})->name('ajout');

$app->get('/supprimer',function() use ($app){
    $control = new Controleur($app);
    $control->supprimerArticlePanier($app->request()->get('id'));
})->name('supprimer');

$app->get('/gestionnaire',function() use ($app){
    $control = new Controleur($app);
    $control->afficherFormulairePrestation("","");
})->name('gestion');

$app->post('/ajoutprestation',function() use ($app){
    $control = new Controleur($app);
    $control->ajoutPrestationBase();
})->name('ajoutprestation');

$app->post('/supprprestation',function() use ($app){
    $control = new Controleur($app);
    $control->supprimerPrestationBase();
})->name('supprimerprestation');

$app->post('/modifprestation',function() use ($app){
    $control = new Controleur($app);
    $control->modifierPrestationBase();
})->name('modifprestation');

$app->get('/cadeau-coffret/:idCoff',function($idCoff) use ($app){
    $tok = $_GET['token'];
    $control = new Controleur($app);
    $control->afficherCoffret($idCoff,$tok);
})->name('cadeau-coffret');

$app->get('/url',function() use ($app){
    $control = new Controleur($app);
    $control->afficherUrl();
})->name('urlCoffret');

$app->get('/notation/:id/:note',function($id,$note) use ($app){
    $control = new Controleur($app);
    $control->mettreNote($id, $note);
});

$app->get('/prestationOfferte/:token/:idCoffret/:idPrest',function($t,$idc,$idp) use ($app){
    $control = new Controleur($app);
    $control->afficherPrestOfferte($t,$idc,$idp);
})->name('prestationOfferte');

$app->run();