SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `prestation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `descr` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `img` text NOT NULL,
  `prix` decimal(5,2) NOT NULL,
  `active` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

INSERT INTO `prestation` (`id`, `nom`, `descr`, `cat_id`, `img`, `prix`,`active`) VALUES
(1, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 1, 'champagne.jpg', '20.00',NULL),
(2, 'Musique', 'Partitions de piano à 4 mains', 1, 'musique.jpg', '25.00',NULL),
(3, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 2, 'poirelregarder.jpg', '14.00',NULL),
(4, 'Goûter', 'Goûter au FIFNL', 3, 'gouter.jpg', '20.00',NULL),
(5, 'Projection', 'Projection courts-métrages au FIFNL', 2, 'film.jpg', '10.00',NULL),
(6, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 1, 'rose.jpg', '16.00',NULL),
(7, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', 3, 'bonroi.jpg', '60.00',NULL),
(8, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 3, 'origami.jpg', '12.00',NULL),
(9, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 1, 'bricolage.jpg', '24.00',NULL),
(10, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 3, 'grandrue.jpg', '59.00',NULL),
(11, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 2, 'place.jpg', '11.00',NULL),
(12, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 1, 'bijoux.jpg', '29.00',NULL),
(13, 'Opéra', 'Concert commenté à l’Opéra', 2, 'opera.jpg', '15.00',NULL),
(14, 'Thé Hotel de la reine', 'Thé de debriefing au bar de l’Hotel de la reine', 3, 'hotelreine.gif', '5.00',NULL),
(15, 'Jeu connaissance', 'Jeu pour faire connaissance', 2, 'connaissance.jpg', '6.00',NULL),
(16, 'Diner', 'Diner (Apéritif / Plat / Vin / Dessert / Café)', 3, 'diner.jpg', '40.00',NULL),
(17, 'Cadeaux individuels', 'Cadeaux individuels sur le thème de la soirée', 1, 'cadeaux.jpg', '13.00',NULL),
(18, 'Animation', 'Activité animée par un intervenant extérieur', 2, 'animateur.jpg', '9.00',NULL),
(19, 'Jeu contacts', 'Jeu pour échange de contacts', 2, 'contact.png', '5.00',NULL),
(20, 'Cocktail', 'Cocktail de fin de soirée', 3, 'cocktail.jpg', '12.00',NULL),
(21, 'Star Wars', 'Star Wars - Le Réveil de la Force. Séance cinéma 3D', 2, 'starwars.jpg', '12.00',NULL),
(22, 'Concert', 'Un concert à Nancy', 2, 'concert.jpg', '17.00',NULL),
(23, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 4, 'apparthotel.jpg', '56.00',NULL),
(24, 'Hôtel d''Haussonville', 'Hôtel d''Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 4, 'hotel_haussonville_logo.jpg', '169.00',NULL),
(25, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 2, 'boitedenuit.jpg', '32.00',NULL),
(26, 'Planètes Laser', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', 2, 'laser.jpg', '15.00',NULL),
(27, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l''élastique inversé, Toboggan géant... et bien plus encore.', 2, 'fort.jpg', '25.00',NULL),
(28, 'gollum', 'Une nuit avec Gollum', 2, 'Gollum.jpg', '122.00', NULL);

CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Attention'),
(2, 'Activité'),
(3, 'Restauration'),
(4, 'Hébergement');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `role` (
  `roleid` int(3) NOT NULL AUTO_INCREMENT,
  `label` varchar(32) NOT NULL,
  `auth_level` int(5) NOT NULL,
  PRIMARY KEY(`roleid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `role` (`roleid`, `label`, `auth_level`) VALUES
(1, 'Utilisateur', 1),
(2, 'Gestionnaire', 100);

CREATE TABLE `utilisateur` (
  `uid` int(5) NOT NULL,
  `pseudonyme` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `roleid` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`uid`);
  ALTER TABLE `utilisateur`
  MODIFY `uid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS coffret(
    idCoffret int(5) NOT NULL AUTO_INCREMENT,
    prixTotal decimal(6,2) NOT NULL,
    restePaye decimal(6,2),
    token text NOT NULL,
    PRIMARY KEY(idCoffret)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS contenuCoffret(
    idContenu int(5) NOT NULL AUTO_INCREMENT,
    qte int(2) NOT NULL,
    idPrestation int(11) NOT NULL,
    idCoffret int(5) NOT NULL,
    PRIMARY KEY(idContenu)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idprest` int(11) NOT NULL,
  `idcateg` int(11) NOT NULL,
  `note` int(11) NOT NULL,
    PRIMARY KEy(id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `note` (`id`, `idprest`, `idcateg`, `note`) VALUES
(1, 9, 1, 5),
(2, 10, 3, 5),
(3, 3, 2, 5),
(4, 3, 2, 5),
(5, 3, 2, 5),
(6, 3, 2, 5),
(7, 3, 2, 5),
(8, 3, 2, 5),
(9, 3, 2, 5),
(10, 3, 2, 5),
(11, 3, 2, 5),
(12, 3, 2, 5),
(13, 3, 2, 5),
(14, 3, 2, 5),
(15, 3, 2, 5),
(16, 10, 3, 5),
(17, 9, 1, 4),
(18, 1, 1, 2),
(19, 1, 1, 3),
(20, 1, 1, 3),
(21, 2, 1, 4),
(22, 3, 2, 5),
(23, 4, 3, 3),
(24, 5, 2, 4),
(25, 6, 1, 4),
(26, 12, 1, 3),
(27, 11, 2, 2),
(28, 10, 3, 4),
(29, 9, 1, 1),
(30, 7, 3, 4),
(31, 13, 2, 5),
(32, 14, 3, 4),
(33, 15, 2, 2),
(34, 16, 3, 5),
(35, 17, 1, 5),
(36, 21, 2, 4),
(37, 20, 3, 4),
(38, 25, 2, 4),
(39, 26, 2, 3),
(40, 27, 2, 3),
(41, 28, 2, 3),
(42, 23, 4, 4),
(43, 24, 4, 4),
(44, 18, 2, 2),
(45, 28, 2, 5),
(46, 28, 2, 3),
(47, 28, 2, 2),
(48, 28, 2, 4),
(49, 27, 2, 5),
(50, 27, 2, 4),
(51, 27, 2, 3),
(52, 27, 2, 2),
(53, 27, 2, 3),
(54, 27, 2, 4),
(55, 26, 2, 1),
(56, 26, 2, 3),
(57, 26, 2, 4),
(58, 26, 2, 3),
(59, 25, 2, 5),
(60, 25, 2, 4),
(61, 25, 2, 4),
(62, 25, 2, 3),
(63, 25, 2, 4),
(64, 25, 2, 2),
(65, 19, 2, 5),
(66, 19, 2, 4),
(67, 19, 2, 1),
(68, 19, 2, 3),
(69, 20, 3, 5),
(70, 20, 3, 4),
(71, 20, 3, 3),
(72, 20, 3, 4),
(73, 20, 3, 2),
(74, 20, 3, 3),
(75, 21, 2, 5),
(76, 21, 2, 4),
(77, 21, 2, 3),
(78, 21, 2, 4),
(79, 21, 2, 2),
(80, 21, 2, 3),
(81, 22, 2, 5),
(82, 22, 2, 4),
(83, 22, 2, 3),
(84, 22, 2, 2),
(85, 22, 2, 3),
(86, 22, 2, 4),
(87, 22, 2, 2),
(88, 23, 4, 5),
(89, 23, 4, 4),
(90, 23, 4, 3),
(91, 23, 4, 4),
(92, 23, 4, 2),
(93, 24, 4, 5),
(94, 24, 4, 4),
(95, 24, 4, 3),
(96, 24, 4, 1),
(97, 24, 4, 4),
(98, 18, 2, 5),
(99, 18, 2, 4),
(100, 18, 2, 1),
(101, 18, 2, 2),
(102, 17, 1, 5),
(103, 17, 1, 4),
(104, 17, 1, 3),
(105, 17, 1, 4),
(106, 16, 3, 2),
(107, 16, 3, 3),
(108, 16, 3, 4),
(109, 16, 3, 2),
(110, 15, 2, 4),
(111, 15, 2, 3),
(112, 15, 2, 4),
(113, 14, 3, 5),
(114, 14, 3, 4),
(115, 14, 3, 3),
(116, 14, 3, 1),
(117, 14, 3, 4),
(118, 13, 2, 5),
(119, 13, 2, 5),
(120, 13, 2, 4),
(121, 13, 2, 1),
(122, 7, 3, 1),
(123, 7, 3, 2),
(124, 7, 3, 3),
(125, 8, 3, 5),
(126, 8, 3, 4),
(127, 8, 3, 3),
(128, 8, 3, 4),
(129, 9, 1, 2),
(130, 9, 1, 1),
(131, 9, 1, 1),
(132, 9, 1, 3),
(133, 9, 1, 5),
(134, 10, 3, 4),
(135, 10, 3, 3),
(136, 10, 3, 3),
(137, 10, 3, 2),
(138, 10, 3, 5),
(139, 10, 3, 4),
(140, 10, 3, 5),
(141, 11, 2, 5),
(142, 11, 2, 4),
(143, 11, 2, 3),
(144, 11, 2, 2),
(145, 11, 2, 3),
(146, 12, 1, 4),
(147, 12, 1, 3),
(148, 12, 1, 3),
(149, 6, 1, 5),
(150, 6, 1, 3),
(151, 6, 1, 2),
(152, 6, 1, 3),
(153, 6, 1, 4),
(154, 5, 2, 4),
(155, 5, 2, 5),
(156, 3, 2, 5),
(157, 3, 2, 4),
(158, 3, 2, 3),
(159, 3, 2, 2),
(160, 2, 1, 5),
(161, 2, 1, 4),
(162, 2, 1, 3),
(163, 2, 1, 2),
(164, 2, 1, 4),
(165, 1, 1, 5),
(166, 1, 1, 4),
(167, 1, 1, 1),
(168, 4, 3, 5);

ALTER TABLE contenuCoffret
  ADD CONSTRAINT coffret_ctrtPrestation FOREIGN KEY (idPrestation) REFERENCES prestation (id);
  ALTER TABLE contenuCoffret
  ADD CONSTRAINT coffret_ctrtCoffret FOREIGN KEY (idCoffret) REFERENCES coffret (idCoffret);

CREATE TABLE `resultat` (
  `idPrestation` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `nbNotes` int(11) NOT NULL,
  `idcateg` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `resultat`
  ADD PRIMARY KEY (`idPrestation`);
ALTER TABLE resultat
  ADD CONSTRAINT resultat_ctrtPrestation FOREIGN KEY (idPrestation) REFERENCES prestation (id);
  ALTER TABLE resultat
  ADD CONSTRAINT resultat_ctrtCateg FOREIGN KEY (idcateg) REFERENCES categorie (id);

INSERT INTO `resultat` (`idPrestation`, `total`, `nbNotes`, `idcateg`) VALUES
(1, 10, 3, 1),
(2, 18, 5, 1),
(3, 14, 4, 2),
(4, 5, 1, 3),
(5, 9, 2, 2),
(6, 17, 5, 1),
(7, 6, 3, 3),
(8, 16, 4, 3),
(9, 12, 5, 1),
(10, 26, 7, 3),
(11, 17, 5, 2),
(12, 10, 3, 1),
(13, 15, 4, 2),
(14, 17, 5, 3),
(15, 11, 3, 2),
(16, 11, 4, 3),
(17, 16, 4, 1),
(18, 12, 4, 2),
(19, 13, 4, 2),
(20, 21, 6, 3),
(21, 21, 6, 2),
(22, 23, 7, 2),
(23, 18, 5, 4),
(24, 17, 5, 4),
(25, 22, 6, 2),
(26, 11, 4, 2),
(27, 21, 6, 2),
(28, 14, 4, 2);