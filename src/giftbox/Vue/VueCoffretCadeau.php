<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 12/01/2017
 * Time: 11:46
 */

namespace giftbox\Vue;


use giftbox\models\ContenuCoffret;
use giftbox\models\Prestation;

class VueCoffretCadeau
{

    protected $httpRequest;
    protected $tok;
    protected $idCoffretR;

    public function __construct($http,$idC,$t){
        $this->httpRequest=$http;
        $this->tok=$t;
        $this->idCoffretR=$idC;
    }

    public function afficher($id){
        $html = "
        <!DOCTYPE html>
            <html>
            <head>
                <meta charset=\"utf-8\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
                <title>MyGiftBox - Vous avez un Cadeau</title>
                <link rel=\"stylesheet\" href=\"../../BS_Acceuil/assets/bootstrap/css/bootstrap.min.css\">
                <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic\">
                <link rel=\"stylesheet\" href=\"../../BS_Acceuil/assets/fonts/font-awesome.min.css\">
                <link rel=\"stylesheet\" href=\"../../BS_Acceuil/assets/css/user.css\">
                <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css\">
                <link rel=\"stylesheet\" href=\"../../BS_Acceuil/assets/css/Simple-Slider.css\">
                <link rel=\"stylesheet\" href=\"../../css/cadeauCoffret.css\">
            </head>
        
        <body>
            <nav class=\"navbar navbar-default navbar-static-top\">
                <div class=\"container\">
                    <div class=\"navbar-header\"><a class=\"navbar-brand navbar-link\" href=\"#\"><i class=\"glyphicon glyphicon-gift\"></i></a>
                        <button class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navcol-1\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navcol-1\">";
        if (isset($_SESSION['profil'])) {
            $html.="<a href='deconnexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Déconnexion </button></a>";
            if ($_SESSION['profil']['auth_level']==100) {
                $html.="<a href='gestionnaire'><button class=\"btn btn-default navbar-btn\" type=\"button\">Gestion </button></a>";
            }
        }
        else{
            $html.="<a href='../inscription'><button class=\"btn btn-default navbar-btn\" type=\"button\">Inscription </button></a>
                            <a href='../connexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Connexion </button></a>";
        }

        $html.="
                        <ul class=\"nav navbar-nav navbar-right\">
                            <li role=\"presentation\"><a href=\"../accueil\">Accueil </a></li>
                            <li class=\"active\" role=\"presentation\"><a href=\"../catalogue/0/alpha\">Catalogue </a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class=\"jumbotron hero\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-4 col-md-push-7 phone-preview\">
                            <div class=\"iphone-mockup\">
                                <div class=\"screen\"></div>
                            </div>
                        </div>
                        <div class=\"col-lg-offset-0 col-md-5 col-md-offset-0 col-md-pull-3 get-it\">
                            <h1>My GiftBox</h1>
                            <p>Offrez sans limite.</p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>";

        $html .= $this->afficherCoffret($id);


        $html .= "<footer class=\"site-footer\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <h5> THENOT - LERAT - JACQUEMIN - GREPIN © 2016 - 2017</h5></div>
                    <div class=\"col-sm-6 social-icons\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></div>
                </div>
            </div>
        </footer>
        <script src=\"BS_Acceuil/assets/js/jquery.min.js\"></script>
        <script src=\"BS_Acceuil/assets/bootstrap/js/bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js\"></script>
        <script src=\"BS_Acceuil/assets/js/Simple-Slider.js\"></script>
        </body>
        </html>";
        return $html;
    }

    public function afficherErreur(){
        return "<div class = \"txt\"><h1>Erreur 404</h1> Votre Url est invalide, revérifier le pour accèder à votre cadeau</div>";
    }

    public function afficherCoffret($id){
        $tab=array();
        $lp = ContenuCoffret::select('idPrestation')->where('idCoffret','=',"$id")->get();
        $html = "<div class = \"classCatal\">";
        foreach($lp as $p){
            $tmp = $p['idPrestation'];
            $pp = Prestation::select('id','nom','descr','img','prix','active')->where('id','=',"$tmp")->first();
            $html .= "<div class = \"imgCatalogue\"><a href=\"../prestationOfferte/$this->tok/$this->idCoffretR/$pp->id\"><img src='../../img/$pp->img'/></a>";
            $html .= "<p id='imgNom'>".$pp->nom ."</p></div>";
            $tab[]=$pp->id;
        }

        return $html;
    }

}