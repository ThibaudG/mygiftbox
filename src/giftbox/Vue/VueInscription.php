<?php

namespace giftbox\Vue;

use giftbox\Vue\VueGlobale;

class VueInscription
{	
	public function afficher($att){
            $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();
            
        	$html.='
						<div class="register-photo">
							<div class="form-container">
								<div class="image-holder"></div>
								<form method="post" action=register>
									<h2 class="text-center"><strong>Création</strong> de compte</h2>
									<div class="form-group">
										<input class="form-control" type="email" name="email" placeholder="Email">
									</div>
									<div class="form-group">
										<input class="form-control" type="password" name="password" placeholder="Mot de passe">
									</div>
									<div class="form-group">
										<input class="form-control" type="password" name="password-repeat" placeholder="Mot de passe à nouveau">
									</div>
									<div class="form-group">
										<div class="checkbox">
											<label class="control-label">
												<input type="checkbox">J\'accepte les conditions d\'utilisations.</label>
										</div>
									</div>
									<div class="form-group">
										<button class="btn btn-primary btn-block" type="submit">S\'inscrire</button>
									</div><a href="connexion" class="already">Vous avez déja un compte ? Connectez vous ici.</a></form>
							</div>
    					</div>';
                        if (isset($att)) {
                            $html.=$att;
                        }
		$html.="</body></html>";
		return $html;
	}

    public function afficherInscription($att){
        return $this->afficher($att);
    }
}
