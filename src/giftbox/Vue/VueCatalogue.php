<?php

namespace giftbox\Vue;

use giftbox\models\Prestation;
use giftbox\models\Categorie;


/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 14/12/2016
 * Time: 14:39
 */
class VueCatalogue{

    protected $httpRequest;


    public function __construct($http){
        $this->httpRequest=$http;
    }

    public function afficher($listePresta,$triPar){
        $html = "
        <!DOCTYPE html>
            <html>
            <head>
                <meta charset=\"utf-8\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
                <title>MyGiftBox - Catalogue</title>
                <link rel=\"stylesheet\" href=\"../../../BS_Acceuil/assets/bootstrap/css/bootstrap.min.css\">
                <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic\">
                <link rel=\"stylesheet\" href=\"../../../BS_Acceuil/assets/fonts/font-awesome.min.css\">
				<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css\">
                <link rel=\"stylesheet\" href=\"../../../BS_Acceuil/assets/css/user.css\">
                <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css\">
                <link rel=\"stylesheet\" href=\"../../../BS_Acceuil/assets/css/Simple-Slider.css\">
                <link rel=\"stylesheet\" href=\"../../../css/styleVueCatalogue.css\">
            </head>
        
        <body>
            <nav class=\"navbar navbar-default navbar-static-top\">
                <div class=\"container\">
                    <div class=\"navbar-header\"><a class=\"navbar-brand navbar-link\" href=\"#\"><i class=\"glyphicon glyphicon-gift\"></i></a>
                        <button class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navcol-1\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navcol-1\">";
                        if (isset($_SESSION['profil'])) {
                            $html.="<a href='deconnexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Déconnexion </button></a>";
                            if ($_SESSION['profil']['auth_level']==100) {
                                $html.="<a href='gestionnaire'><button class=\"btn btn-default navbar-btn\" type=\"button\">Gestion </button></a>";       
                            }
                        }
                        else{
                            $html.="<a href='../../inscription'><button class=\"btn btn-default navbar-btn\" type=\"button\">Inscription </button></a>
                            <a href='../../connexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Connexion </button></a>";
                        }

                        $html.="
                        <ul class=\"nav navbar-nav navbar-right\">
                            <li role=\"presentation\"><a href=\"../../accueil\">Accueil </a></li>
                            <li class=\"active\" role=\"../presentation\"><a href=\"../../catalogue/0/alpha\">Catalogue </a></li>
                            <li role=\"presentation\"><a href=\"../../panier\">Panier </a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class=\"jumbotron hero\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-4 col-md-push-7 phone-preview\">
                            <div class=\"iphone-mockup\">
                                <div class=\"screen\"></div>
                            </div>
                        </div>
                        <div class=\"col-lg-offset-0 col-md-5 col-md-offset-0 col-md-pull-3 get-it\">
                            <h1>My GiftBox</h1>
                            <p>Offrez sans limite.</p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div><br />
			<h4>Catégories </h4><br />
			<div class=\"btn-group btn-group-justified\" role=\"group\"><a class=\"btn btn-default\" role=\"button\" href=\"../../catalogue/0/alpha\">Tout</a>
			<a class=\"btn btn-default\" role=\"button\" href=\"../../catalogue/1/alpha\" data-bs-hover-animate=\"pulse\">Attention </a><a class=\"btn btn-default\" role=\"button\" href=\"../../catalogue/2/alpha\" data-bs-hover-animate=\"pulse\">Activité </a>
			<a class=\"btn btn-default\" role=\"button\" href=\"../../catalogue/3/alpha\" data-bs-hover-animate=\"pulse\">Restauration </a><a class=\"btn btn-default\" role=\"button\" href=\"../../catalogue/4/alpha\" data-bs-hover-animate=\"pulse\">Hébergement </a></div><br />
			<br />
			<div class=\"btn-group\" role=\"group\">
				<div class=\"dropdown btn-group\" role=\"group\">
					<button class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\" type=\"button\">Tri par prix</button>
						<ul class=\"dropdown-menu\" role=\"menu\">
							<li role=\"presentation\"><a href=\"croiss\">Prix croissants</a></li>
							<li role=\"presentation\"><a href=\"decroiss\">Prix decroissants</a></li>
						</ul>
					</div>
				</div>
			";

        $html .= "<div class = \"classCatal\">";
        foreach($listePresta as $p){
            if($p->active==null){
            $html .= "<div class = \"imgCatalogue\"><a href=\"../../prestation/$p->id\"><img src='../../../img/$p->img'/></a>";
            $html .= "<p id='imgNom'>".$p->nom ."<br/>Prix : ".$p->prix ."€&nbsp;&nbsp;&nbsp;<a href='../../ajout/".$p->id."/$triPar'>Ajouter</a></p></div>";
            }
        }

        $html .= '</div> <br/>';
        /**foreach ($_GET as $p => $v) {
            $html .= "Affichage de la prestation $v <br/>";
            $lp = Prestation::select('id', 'nom', 'descr', 'img', 'prix')
                ->where('id', '=', "$v")
                ->get();
            foreach ($lp as $p) {
                $html .= "(" . $p->id . ") " . $p->nom . " : " . $p->descr . " : $p->img : $p->prix " . "<br/>";
            }
            $html .= "<br/>";
        }*/
        $html .= "<footer class=\"site-footer\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <h5> THENOT - LERAT - JACQUEMIN - GREPIN © 2016 - 2017</h5></div>
                    <div class=\"col-sm-6 social-icons\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></div>
                </div>
            </div>
        </footer>
        <script src=\"../../../BS_Acceuil/assets/js/jquery.min.js\"></script>
        <script src=\"../../../BS_Acceuil/assets/bootstrap/js/bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js\"></script>
		<script src=\"../../../BS_Acceuil/assets/js/bs-animation.js\"></script>
        </body>
        </html>";
        return $html;
    }

    public function __get($attName) {
        if(property_exists($this, $attName))
            return $this->$attName;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

    public function __set($attName, $value) {
        if(property_exists($this, $attName))
            $this->$attName = $value;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

    /*
    * permet d'afficher la liste des prestation correspondant a chaque catégorie
    */
    public function afficher_liste_prest_categ(){
    	//$lp=Prestation::where('cat_id','=',2)->get();
        $l=Categorie::orderBy('id')->get();
        foreach ($l as $lis) {
            echo $lis->nom.'</br>';
            $lp=$lis->prestation;
    	   foreach ($lp as $yolo) {
            //opération d'affichage
    	   echo $yolo->id.'     ';
            }
            echo "</br>";
    	}
    }

    /**
    * fonction permettant d'afficher la liste des catégories
    */
    public function afficher_liste_categ(){
        $l=Categorie::orderBy('id')->get();
        foreach ($l as $lis) {
            echo $lis->nom.'</br>';
            $lp=$lis->prestation;
        }
    }

    public function afficher_details_prest($idprest){
        $l=Prestation::where('id','=',$idprest)->first();
        echo $l->id."    ".$l->nom."    ".$l->descr."    ".$l->cat_id."    ".$l->img."    ".$l->prix."€";
    }

    /**
    * permet d'afficher la liste des prestation du prix
    * du plus bas au plus haut
    */
    public function afficher_trie_prix(){
        $lp = Prestation::orderBy('prix')->get();
        foreach($lp as $prest){
            echo $prest->nom."    ".$prest->prix. "<br/>";
        }
    }

/**
    public function ajouter_prestation($prest){
        if (isset($_SESSION['panier'])){
            $tab=$_SESSION['panier'];
            if(isset($tab[$prest])){
                $tab[$prest]=$tab[$prest]+1;
            }
            else{
                $tab[$prest]=1;
            }
            $_SESSION['panier']=$tab;
        }
        else{
            $_SESSION['panier'][$prest]=1;
        }
        echo $this->afficher();
    }
    */
}