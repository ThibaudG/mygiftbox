<?php

namespace giftbox\Vue;

use giftbox\Vue\VueGlobale;

class VueConnexion{

    public function afficher($att){
            $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();
            $html.='
                        <div class="login-clean">
                            <form method="post" action=signin>
                                <h2 class="text-center">Connectez vous !</h2>
                                <div class="illustration"><i class="icon ion-cube"></i></div>
                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Mot de passe">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Connexion</button>
                                </div>
                                </form>
                        </div>';
                        if (isset($att)) {
                            $html.=$att;
                        }
        $html.=$vueG->end();
        return $html;
    }

    public function afficherConnexion($att){
        return $this->afficher($att);
    }

    public function afficherEtoiles(){
        $h="yolo";
        //$h="<a><img src='../../../../img/film.jpg'/></a>";
        return $h;
    }
}
