<?php
namespace giftbox\Vue;

class VueGlobale{
	protected $httpRequest;

	public function __construc($http){
		$this->httpRequest=$http;
	}

    public function yolo(){
        return "tolo";
    }
		
    public function head(){
        $head = "
        <!DOCTYPE html>
            <html>
            <head>
                <meta charset=\"utf-8\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
                <title>MyGiftBox</title>
                <link rel=\"stylesheet\" href=\"../BS_Acceuil/assets/bootstrap/css/bootstrap.min.css\">
                <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic\">
                <link rel=\"stylesheet\" href=\"../BS_Acceuil/assets/fonts/font-awesome.min.css\">
                <link rel=\"stylesheet\" href=\"../BS_Acceuil/assets/css/user.css\">
                <link rel=\"stylesheet\" href=\"../BS_Acceuil/assets/fonts/ionicons.min.css\">
                <link rel=\"stylesheet\" href=\"../BS_Acceuil/assets/css/Login-Form-Clean.css\">
                <link rel=\"stylesheet\" href=\"../BS_Acceuil/assets/css/Registration-Form-with-Photo2.css\">
                <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css\">
                <link rel=\"stylesheet\" href=\"../BS_Acceuil/assets/css/Simple-Slider.css\">
                <link rel=\"stylesheet\" href=\"../css/styleVueCatalogue.css\">
                <link rel=\"stylesheet\" href=\"../css/panier.css\">
                <link rel=\"stylesheet\" href=\"../css/payement.css\">
                <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css\">
            </head>";
        return $head;
    }

    public function body(){
        $body="
        <body>
            <nav class=\"navbar navbar-default navbar-static-top\">
                <div class=\"container\">
                    <div class=\"navbar-header\"><a class=\"navbar-brand navbar-link\" href=\"#\"><i class=\"glyphicon glyphicon-gift\"></i></a>
                        <button class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navcol-1\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navcol-1\">";
                        if (isset($_SESSION['profil'])) {
                            $body.="<a href='deconnexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Déconnexion </button></a>";
                            if ($_SESSION['profil']['auth_level']==100) {
                                $body.="<a href='gestionnaire'><button class=\"btn btn-default navbar-btn\" type=\"button\">Gestion </button></a>";       
                            }
                        }
                        else{
                            $body.="<a href='inscription'><button class=\"btn btn-default navbar-btn\" type=\"button\">Inscription </button></a>
                            <a href='connexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Connexion </button></a>";
                        }

                        $body.="
                        <ul class=\"nav navbar-nav navbar-right\">
                            <li role=\"presentation\"><a href=\"accueil\">Accueil </a></li>
                            <li class=\"active\" role=\"presentation\"><a href=\"catalogue/0/alpha\">Catalogue </a></li>
                            <li role=\"presentation\"><a href=\"panier\">Panier </a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class=\"jumbotron hero\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-4 col-md-push-7 phone-preview\">
                            <div class=\"iphone-mockup\">
                                <div class=\"screen\"></div>
                            </div>
                        </div>
                        <div class=\"col-lg-offset-0 col-md-5 col-md-offset-0 col-md-pull-3 get-it\">
                            <h1>My GiftBox</h1>
                            <p>Offrez sans limite.</p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>";
            return $body;
    }

    public function end(){
        $end="</body></html>";
        return $end;
    }
}