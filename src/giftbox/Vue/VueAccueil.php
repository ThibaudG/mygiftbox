<?php

namespace giftbox\Vue;

use giftbox\models\Prestation;
use giftbox\Vue\VueGlobale;
use giftbox\Vue\VueGlobale2;
use giftbox\models\Resultat;
use giftbox\models\Categorie;
class VueAccueil{

	protected $httpRequest;
	
	public function __construct($http){
		$this->httpRequest=$http;
	}
	
	public function afficher($listeMeilleurs){

        $lp=array();
        foreach ($listeMeilleurs as $k=>$v){
            $lp[] = Prestation::where("id","like","$v")->first();
        }
        $i1 = $lp[0];
        $v1=  $i1->img;
        $i2 = $lp[1];
        $v2=  $i2->img;
        $i3 = $lp[2];
        $v3=  $i3->img;
        $i4 = $lp[3];
        $v4=  $i4->img;

        $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();
            $html.="<section class=\"testimonials\">
				<h2 class=\"text-center\">Vous allez aimer !</h2>
				<p class=\"lead text-center\">Choisissez parmi un grand nombre de cadeaux celui qui correspondra le mieux à votre ami, famille ou autre !! <br/>
				Decouvrer dès maintenant nos meilleurs prestations comme des $i1->descr , $i2->descr ,$i3->descr , ou encore $i4->descr. </p>
			</section>";

			$html .= "<div class=\"simple-slider\">
				<div class=\"swiper-container\">
					<div class=\"swiper-wrapper\">
						<div class=\"swiper-slide\" style=\"background-image:url(../img/$v1);\"></div>
                        <div class=\"swiper-slide\" style=\"background-image:url(../img/$v2);\"></div>
                        <div class=\"swiper-slide\" style=\"background-image:url(../img/$v3);\"></div>
                        <div class=\"swiper-slide\" style=\"background-image:url(../img/$v4);\"></div>
					</div>
					<div class=\"swiper-pagination\"></div>
					<div class=\"swiper-button-prev\"></div>
					<div class=\"swiper-button-next\"></div>
				</div>
			</div>
			<footer class=\"site-footer\">
				<div class=\"container\">
					<div class=\"row\">
						<div class=\"col-sm-6\">
							<h5> THENOT - LERAT - JACQUEMIN - GREPIN © 2016 - 2017</h5></div>
						<div class=\"col-sm-6 social-icons\"><a href=\"https://www.facebook.com/mygiftboxfoundation/\"><i class=\"fa fa-facebook\"></i></a></div>
					</div>
				</div>
			</footer>
			<script src=\"../BS_Acceuil/assets/js/jquery.min.js\"></script>
			<script src=\"../BS_Acceuil/assets/bootstrap/js/bootstrap.min.js\"></script>
			<script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js\"></script>
			<script src=\"../BS_Acceuil/assets/js/Simple-Slider.js\"></script>";
		    $html.=$vueG->end();


        return $html;
	}

	public function afficher2(){
		    $vueG=new VueGlobale2();
            $html=$vueG->head();
            $html.=$vueG->body();
            $html.="<section class=\"testimonials\">
				<h2 class=\"text-center\">Vous allez aimer !</h2>
				<p class=\"lead text-center\">Choisissez parmi un grand nombre de cadeaux celui qui correspondra le mieux à votre ami, famille ou autre.</p>
			</section>
			<div class=\"simple-slider\">
				<div class=\"swiper-container\">
					<div class=\"swiper-wrapper\">
						<div class=\"swiper-slide\" style=\"background-image:url(../../../BS_Acceuil/assets/img/italian-restaurant-slideshow-lead.jpg);\"></div>
						<div class=\"swiper-slide\" style=\"background-image:url(../../../BS_Acceuil/assets/img/slide-parcours-ecureuil-03.jpg);\"></div>
						<div class=\"swiper-slide\" style=\"background-image:url(../../../BS_Acceuil/assets/img/disco.jpg);\"></div>
					</div>
					<div class=\"swiper-pagination\"></div>
					<div class=\"swiper-button-prev\"></div>
					<div class=\"swiper-button-next\"></div>
				</div>
			</div>
			<footer class=\"site-footer\">
				<div class=\"container\">
					<div class=\"row\">
						<div class=\"col-sm-6\">
							<h5> THENOT - LERAT - JACQUEMIN - GREPIN © 2016 - 2017</h5></div>
						<div class=\"col-sm-6 social-icons\"><a href=\"https://www.facebook.com/mygiftboxfoundation/\"><i class=\"fa fa-facebook\"></i></a></div>
					</div>
				</div>
			</footer>
			<script src=\"../../../BS_Acceuil/assets/js/jquery.min.js\"></script>
			<script src=\"../../../BS_Acceuil/assets/bootstrap/js/bootstrap.min.js\"></script>
			<script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js\"></script>
			<script src=\"../../../BS_Acceuil/assets/js/Simple-Slider.js\"></script>";
		    $html.=$vueG->end();


        return $html;
	}
	
}