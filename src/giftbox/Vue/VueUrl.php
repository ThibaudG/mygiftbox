<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 13/01/2017
 * Time: 08:40
 */

namespace giftbox\Vue;


class VueUrl
{

    protected $nom;
    protected $httpRequest;

    public function __construct($http){
        $this->httpRequest=$http;
    }

    public function afficher($url){
        $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();

        $html .= "<div style=text-align:center;><h2>Paiement accepté, envoyez dès à présent votre coffret grâce à cette url : </h2> </br> 
                    <p style=font-size:20px;>$url</p> </div>";

        $html .= "<footer class=\"site-footer\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <h5> THENOT - LERAT - JACQUEMIN - GREPIN © 2016 - 2017</h5></div>
                    <div class=\"col-sm-6 social-icons\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></div>
                </div>
            </div>
        </footer>
        <script src=\"BS_Acceuil/assets/js/jquery.min.js\"></script>
        <script src=\"BS_Acceuil/assets/bootstrap/js/bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js\"></script>
        <script src=\"BS_Acceuil/assets/js/Simple-Slider.js\"></script>";
            $html.=$vueG->end();
        return $html;
    }

    public function __get($attName) {
        if(property_exists($this, $attName))
            return $this->$attName;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

    public function __set($attName, $value) {
        if(property_exists($this, $attName))
            $this->$attName = $value;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

}