<?php
namespace giftbox\Vue;

use giftbox\models\Coffret;
use giftbox\models\ContenuCoffret;
use giftbox\models\Prestation;
use giftbox\models\Categorie;

class VueRecapitulatif{
	protected $httpRequest;

	public function __construct($http){
		$this->httpRequest=$http;
	}

	public function afficher(){
         $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();
		    $html.="<div class=\"contenu\">;
                <table style=\"width: 100%\">
                    <tr>
                        <td colspan=\"4\" class=\"panierName\">Votre panier</td>
                    </tr>";

            $html.=$this->afficherRecap();
            $html.=$vueG->end();

		return $html;
		}

	public function afficherRecap(){
		$r="";
		if(!isset($_SESSION['panier'])){
			$r.="<tr><td class=\"titreTab\">/!\ Le panier est vide /!\</td></tr>";
		}else{
            $nbArt = count($_SESSION['panier']);
            $nbCat = [];
            $prixTot = 0;
            foreach ($_SESSION['panier'] as $key => $value) {
                $panier = Prestation::select('id','cat_id','prix')->where('id','=',$key)->first();
                $prixTot += $panier->prix;
                if(!isset($nbCat[$panier->cat_id])){
                    $nbCat += [$panier->cat_id => $panier->cat_id];
                }
            }
        if($nbArt < 2 || count($nbCat) < 2){
            $r.="</table><p class=\"nonValid\">Panier non valide, au moins deux article et deux categorie</p>";
        }else{
    		$r.= "<tr>
        	<td class=\"titreTab\">Nombre d'article</td>
        	<td class=\"titreTab\">Nombre de catégorie</td>
        	<td class=\"titreTab\">Prix total</td>
        	</tr>";
        	$r.= "<tr>
        	<td>$nbArt</td>
        	<td>".count($nbCat)."</td>
        	<td>$prixTot €</td>
        	</tr></table>";
            $r.=$this->afficher_boutton();
        }
		}
		return $r;
	}

    public function afficher_boutton(){
        $r="<form action=\"payement\" class=\"final\" method=\"post\">
                <input type=\"radio\" name=\"id\" value=\"1\" checked> Pot commun<br>
                <input type=\"radio\" name=\"id\" value=\"2\"> Individuel<br>
                <input type=\"submit\" value=\"Payement\" />
            </form>"; 
        return $r;
    }

}