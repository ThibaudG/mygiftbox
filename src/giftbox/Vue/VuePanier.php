<?php

namespace giftbox\Vue;

use giftbox\models\Prestation;
use giftbox\models\Categorie;
use giftbox\Vue\VueGlobale;

class VuePanier{
	protected $httpRequest;

	public function __construct($http){
		$this->httpRequest=$http;
	}

	public function afficher(){
        $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();
            $html.="
            <div class=\"contenu\">
                <table style=\"width: 100%\">
                    <tr>
                        <td colspan=\"4\" class=\"panierName\">Votre panier
                    </tr>";
		$html.=$this->afficher_panier()."</table>";
        $html.=$this->afficher_boutton();
        $html.="</body></html>";
		return $html;
	}
	
	public function afficher_panier(){
        $r="";
        if(!isset($_SESSION['panier'])){
            $r.="<tr><td class=\"titreTab\">/!\ Le panier est vide /!\</td></tr>";
        }else{
            $r.= "<tr>
            <td class=\"titreTab\">Libellé</td>
            <td class=\"titreTab\">Quantité</td>
            <td class=\"titreTab\">Prix Unitaire</td>
            <td class=\"titreTab\">Image</td>
            </tr>";
            foreach ($_SESSION['panier'] as $key => $value) {
                $panier = Prestation::select('id','nom','prix','img')->where('id','=',$key)->first();
                $r.="<tr><td>$panier->nom</td>";
                $r.="<td>$value</td>";
                $r.="<td>$panier->prix €</td>";
                $r.="<td><img src=\"../img/$panier->img\"></td>";
                $r.="<td><a href=supprimer?id=$panier->id>Supprimer</a></td>";
                $r.="</tr>";
            }
        }
        return $r;
    }

    public function afficher_boutton(){
        $r="<form action=\"recapitulatif\" class=\"final\">
                <input type=\"submit\" value=\"Finaliser la commande\" />
            </form>"; 
        return $r;
    }
}