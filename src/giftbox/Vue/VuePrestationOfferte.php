<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 10/01/2017
 * Time: 14:31
 */

namespace giftbox\Vue;

use giftbox\models\Note;
use giftbox\models\Prestation;

class VuePrestationOfferte
{

    protected $httpRequest;
    protected $token;
    protected $idCoffret;
    protected $idPrestation;

    public function __construct($http,$t,$idC,$idP){
        $this->httpRequest=$http;
        $this->token=$t;
        $this->idCoffret = $idC;
        $this->idPrestation= $idP;
    }

    public function afficher($idSuivant){
        $html = "
        <!DOCTYPE html>
            <html>
            <head>
                <meta charset=\"utf-8\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
                <title>MyGiftBox</title>
                <link rel=\"stylesheet\" href=\"../../../../BS_Acceuil/assets/bootstrap/css/bootstrap.min.css\">
                <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic\">
                <link rel=\"stylesheet\" href=\"../../../../BS_Acceuil/assets/fonts/font-awesome.min.css\">
                <link rel=\"stylesheet\" href=\"../../../../BS_Acceuil/assets/css/user.css\">
                <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css\">
                <link rel=\"stylesheet\" href=\"../../../../BS_Acceuil/assets/css/Simple-Slider.css\">
                <link rel=\"stylesheet\" href=\"../../../../css/prestation.css\">
            </head>
        
        <body>
            <nav class=\"navbar navbar-default navbar-static-top\">
                <div class=\"container\">
                    <div class=\"navbar-header\"><a class=\"navbar-brand navbar-link\" href=\"#\"><i class=\"glyphicon glyphicon-gift\"></i></a>
                        <button class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navcol-1\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navcol-1\">";
        if (isset($_SESSION['profil'])) {
            $html.="<a href='deconnexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Déconnexion </button></a>";
            if ($_SESSION['profil']['auth_level']==100) {
                $html.="<a href='gestionnaire'><button class=\"btn btn-default navbar-btn\" type=\"button\">Gestion </button></a>";
            }
        }
        else{
            $html.="<a href='../../../inscription'><button class=\"btn btn-default navbar-btn\" type=\"button\">Inscription </button></a>
                            <a href='../../../connexion'><button class=\"btn btn-default navbar-btn\" type=\"button\">Connexion </button></a>";
        }

        $html.="
                        <ul class=\"nav navbar-nav navbar-right\">
							<li class=\"active\" role=\"presentation\"><a href=\"../../../accueil\">Accueil </a></li>
                            <li role=\"presentation\"><a href=\"../../../catalogue/0/alpha\">Catalogue </a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class=\"jumbotron hero\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-4 col-md-push-7 phone-preview\">
                            <div class=\"iphone-mockup\">
                                <div class=\"screen\"></div>
                            </div>
                        </div>
                        <div class=\"col-lg-offset-0 col-md-5 col-md-offset-0 col-md-pull-3 get-it\">
                            <h1>My GiftBox</h1>
                            <p>Offrez sans limite.</p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
			<br />
			<a href=\"../../../cadeau-coffret/$this->idCoffret?token=$this->token\" class=\"btn btn-info\" role=\"button\">Retour Coffret</a><br />
			<a href=\"$idSuivant\" class=\"btn btn-info\" role=\"button\">Suivant</a><br />
			<br />";

        $prest = Prestation::where("id","=","$this->idPrestation")->first();
        $html .= "<div class = \"imgPres\"><img src='../../../../img/$prest->img'/>";
        $html .= "<p id='imgNom'>".$prest->nom ."<br/>$prest->descr </p>";


        $html .= "<footer class=\"site-footer\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <h5> THENOT - LERAT - JACQUEMIN - GREPIN © 2016 - 2017</h5></div>
                    <div class=\"col-sm-6 social-icons\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></div>
                </div>
            </div>
        </footer>
        <script src=\"BS_Acceuil/assets/js/jquery.min.js\"></script>
        <script src=\"BS_Acceuil/assets/bootstrap/js/bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js\"></script>
        <script src=\"BS_Acceuil/assets/js/Simple-Slider.js\"></script>
        </body>
        </html>";
        return $html;
    }

    public function __get($attName) {
        if(property_exists($this, $attName))
            return $this->$attName;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

    public function __set($attName, $value) {
        if(property_exists($this, $attName))
            $this->$attName = $value;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

    /*
    * permet d'afficher la liste des prestation correspondant a chaque catégorie
    */
    public function afficher_liste_prest_categ(){
        //$lp=Prestation::where('cat_id','=',2)->get();
        $l=Categorie::orderBy('id')->get();
        foreach ($l as $lis) {
            echo $lis->nom.'</br>';
            $lp=$lis->prestation;
            foreach ($lp as $yolo) {
                //opération d'affichage
                echo $yolo->id.'     ';
            }
            echo "</br>";
        }
    }

    /**
     * fonction permettant d'afficher la liste des catégories
     */
    public function afficher_liste_categ(){
        $l=Categorie::orderBy('id')->get();
        foreach ($l as $lis) {
            echo $lis->nom.'</br>';
            $lp=$lis->prestation;
        }
    }

    public function afficher_details_prest($idprest){
        $l=Prestation::where('id','=',$idprest)->first();
        echo $l->id."    ".$l->nom."    ".$l->descr."    ".$l->cat_id."    ".$l->img."    ".$l->prix."€";
    }

    /**
     * permet d'afficher la liste des prestation du prix
     * du plus bas au plus haut
     */
    public function afficher_trie_prix(){
        $lp = Prestation::orderBy('prix')->get();
        foreach($lp as $prest){
            echo $prest->nom."    ".$prest->prix. "<br/>";
        }
    }

}