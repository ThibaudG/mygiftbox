<?php

namespace giftbox\Vue;

use giftbox\models\Prestation;
use giftbox\models\Categorie;

/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 14/12/2016
 * Time: 14:39
 */
class VueFormulaireGestionnaire{

    protected $httpRequest;


    public function __construct($http){
        $this->httpRequest=$http;
    }

    public function afficher($a,$b,$c){
        $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();
            $html.='
            <div class="text-center">
                <form id="f1" method="post" action="ajoutprestation">
                    Details de la prestation :
                <br>
                <input type="text"  name="nom_prest"  placeholder="Dénomination">
                <input type="text"  name="desc_prest"   placeholder="Description">
                <input type="number" name="idcateg_prest"    placeholder="ID de la catégorie">
                <input type="text"  name="img_prest" placeholder="Nom de l\'image">
                <input type="number"  name="prix_prest" placeholder="Prix">
                    <button type="submit">
                    Ajouter
                    </button>
                </form>
                </div>';
            $html .= $a;

            $html.='<br><br>
            <div class="text-center"><form id="f2" method="post" action="supprprestation">
            Donner le nom de la prestation a supprimer (case sensitive) :
            <br>
            <input type="text"  name="nom_suppr"  placeholder="Nom">
            <button type="submit">
                Supprimer
            </button>
            </form>
            </div>';

            $html .= $b;

            $html.='<br><br>
            <div class="text-center"><form id="f3" method="post" action="modifprestation">
            Donner le nom de la prestation à désactiver ou à réactiver (case sensitive):
            <br>
            <input type="text"  name="nom_modif"  placeholder="Nom">
            <button type="submit">
                Activer/Désactiver
            </button></form>
            </div>';

            $html.=$c;

            $lp=Prestation::where('active','=',null)->get();
            if (isset($lp)) {
                $html.='<br><br><div class="text-center">Liste des prestation qui sont activées :
                <br><select>';    
                foreach ($lp as $key => $value) {
                    $html.="<option>$value->nom</option>";
                }
                $html.='</select></div>';
            }
            $lp=Prestation::where('active','=','1')->get();
            if (isset($lp)) {
                $html.='<br><br><div class="text-center">Liste des prestations qui sont désactivées :
                <br><select>';
                foreach ($lp as $key => $value) {
                    $html.="<option>$value->nom</option>";
                }
                $html.='</select></div><br>';
            }

        $html .= "</div> <br/><footer class=\"site-footer\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <h5> THENOT - LERAT - JACQUEMIN - GREPIN © 2016 - 2017</h5></div>
                    <div class=\"col-sm-6 social-icons\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></div>
                </div>
            </div>
        </footer>
        <script src=\"../BS_Acceuil/assets/js/jquery.min.js\"></script>
        <script src=\"../BS_Acceuil/assets/bootstrap/js/bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js\"></script>
        <script src=\"../BS_Acceuil/assets/js/Simple-Slider.js\"></script>";
        $html.=$vueG->end();
        return $html;
    }
    public function afficherAjout($a){
        return $this->afficher($a,"","");
    }

    public function afficherSuppr($a){
        return $this->afficher("",$a,"");
    }

    public function afficherModif($a){
        return $this->afficher("","",$a);
    }    
}