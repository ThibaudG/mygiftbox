<?php
namespace giftbox\Vue;

use giftbox\models\Prestation;
use giftbox\models\Categorie;

class VuePayement{
	protected $httpRequest;

	public function __construct($http){
		$this->httpRequest=$http;
	}

	public function afficher(){
		 $vueG=new VueGlobale();
            $html=$vueG->head();
            $html.=$vueG->body();

            if ($_POST['id'] == 2) {
            	$html.=$this->payementIndiv();
            }elseif ($_POST['id'] == 1) {
            	$html.=$this->payementCommun();
            }
            $html.=$vueG->end();
            return $html;
        }

        public function payementIndiv()
        {
        	$r='<div class="row .payment-dialog-row">
        <div class="col-md-4 col-md-offset-4 col-xs-12">
				<div class="panel panel-default credit-card-box">
					<div class="panel-heading">
						<h3 class="panel-title"><span class="panel-title-text">Informations sur le moyen de paiement </span><img class="img-responsive panel-title-image" src="../BS_Acceuil/assets/img/accepted_cards.png"></h3></div>
					<div class="panel-body">
						<form id="payment-form" action="url">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="control-label" for="cardNumber">Numéro de carte </label>
										<div class="input-group">
											<input class="form-control" type="tel" required="" placeholder="Numéro de carte valide" id="cardNumber">
											<div class="input-group-addon"><span><i class="fa fa-credit-card"></i></span></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-7">
									<div class="form-group">
										<label class="control-label" for="cardExpiry"><span class="hidden-xs">Date  </span><span class="visible-xs-inline">EXP </span> d\'expiration</label>
										<input class="form-control" type="tel" required="" placeholder="MM / YY" id="cardExpiry">
									</div>
								</div>
								<div class="col-xs-5 pull-right">
									<div class="form-group">
										<label class="control-label" for="cardCVC">Code de sécurité</label>
										<input class="form-control" type="tel" required="" placeholder="XXX" id="cardCVC">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<button class="btn btn-success btn-block btn-lg" type="submit">Payer</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>';


        	return $r;
        }

        public function payementCommun()
        {
        	$r="";



        	return $r;
        }
    }