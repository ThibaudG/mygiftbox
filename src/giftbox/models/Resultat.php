<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 17/01/2017
 * Time: 22:05
 */

namespace giftbox\models;

use Illuminate\Database\Eloquent\Model;

class Resultat extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'resultat';
    protected $primaryKey = 'idPrestation';
    public $timestamps = false;

}