<?php
namespace giftbox\models;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'utilisateur';
	protected $primaryKey = 'uid';
	public $timestamps = false;
}