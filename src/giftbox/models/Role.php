<?php
namespace giftbox\models;

use Illuminate\Database\Eloquent\Model;

class Role extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'role';
	protected $primaryKey = 'roleid';
	public $timestamps = false;
}