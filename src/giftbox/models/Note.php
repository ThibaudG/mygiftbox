<?php
namespace giftbox\models;

use Illuminate\Database\Eloquent\Model;

class Note extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'note';
	protected $primaryKey = 'id';
	public $timestamps = false;
}