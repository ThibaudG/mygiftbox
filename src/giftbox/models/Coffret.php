<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 12/01/2017
 * Time: 12:44
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

class Coffret extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'coffret';
    protected $primaryKey = 'idCoffret';
    public $timestamps = false;

}