<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 12/01/2017
 * Time: 12:44
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

class ContenuCoffret extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'contenuCoffret';
    protected $primaryKey = 'idContenu';
    public $timestamps = false;

}