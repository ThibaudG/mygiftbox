<?php
namespace giftbox\models;

use Illuminate\Database\Eloquent\Model;

class Prestation extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'prestation';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function categorie(){
		return $this->belongsTo('\giftbox\models\Categorie','cat_id');
	}
}