<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 13/01/2017
 * Time: 22:36
 */

namespace giftbox\Controleur;

use giftbox\Vue\VueCatalogue;
use giftbox\models\Prestation;
use giftbox\models\Categorie;

class ControleurCatalogue
{

    protected $httpRequest;

    public function __construct($http){
        $this->httpRequest = $http;
    }

    public function afficherCatalogue($categNum,$triPar){
        $vue = new VueCatalogue($this->httpRequest);
        $res = null;
        switch($categNum){
            case 0:
                switch($triPar){
                    case "alpha":
                        $res = $this->retournerTout();
                        break;
                    case "croiss":
                        $res = $this->retournerTout($triPar);
                        break;
                    case "decroiss":
                        $res = $this->retournerTout($triPar);
                        break;
                }
                break;
            case 1:
                switch($triPar){
                    case "alpha":
                        $res = $this->retournerCateg($categNum);
                        break;
                    case "croiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                    case "decroiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                }
                break;
            case 2:
                switch($triPar){
                    case "alpha":
                        $res = $this->retournerCateg($categNum);
                        break;
                    case "croiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                    case "decroiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                }
                break;
            case 3:
                switch($triPar){
                    case "alpha":
                        $res = $this->retournerCateg($categNum);
                        break;
                    case "croiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                    case "decroiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                }
                break;
            case 4:
                switch($triPar){
                    case "alpha":
                        $res = $this->retournerCateg($categNum);
                        break;
                    case "croiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                    case "decroiss":
                        $res = $this->retournerCateg($categNum,$triPar);
                        break;
                }
                break;
        }
        echo $vue->afficher($res,$triPar);
    }

    public function retournerTout($triPar = "alpha"){
        switch($triPar){
            case "alpha":
                $lp = Prestation::select('id','nom','descr','img','prix','active')->get();
                break;
            case "croiss":
                $lp = Prestation::select('id','nom','descr','img','prix','active')->orderBy('prix')->get();
                break;
            case "decroiss":
                $lp = Prestation::select('id','nom','descr','img','prix','active')->orderBy('prix','DESC')->get();
                break;
        }
        return $lp;
    }

    public function retournerCateg($idCateg,$triPar="alpha"){
        switch($triPar){
            case "alpha":
                $lp = Prestation::select('id','nom','descr','img','prix','active')->where('cat_id','=',"$idCateg")->get();
                break;
            case "croiss":
                $lp = Prestation::select('id','nom','descr','img','prix','active')->where('cat_id','=',"$idCateg")->orderBy('prix')->get();
                break;
            case "decroiss":
                $lp = Prestation::select('id','nom','descr','img','prix','active')->where('cat_id','=',"$idCateg")->orderBy('prix','DESC')->get();
                break;
        }
        return $lp;
    }

    public function ajouterPrestationCoffret($prest,$triPar){
        if (isset($prest)) {
            if (isset($_SESSION['panier'])){
                $tab=$_SESSION['panier'];
                if(isset($tab[$prest])){
                    $tab[$prest]=$tab[$prest]+1;
                }
                else{
                    $tab[$prest]=1;
                }
                $_SESSION['panier']=$tab;
            }
            else{
                $_SESSION['panier'][$prest]=1;
            }
        }
        echo $this->afficherCatalogue(0,$triPar);
    }

}