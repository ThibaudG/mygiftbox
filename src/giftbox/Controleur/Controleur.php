<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 14/12/2016
 * Time: 15:12
 */

namespace giftbox\Controleur;

use giftbox\Vue\VueAccueil;
use giftbox\Vue\VueCatalogue;
use giftbox\Vue\VueCoffretCadeau;
use giftbox\Vue\VueInscription;
use giftbox\Vue\VueConnexion;
use giftbox\Vue\VuePrestationOfferte;
use giftbox\Vue\VueUrl;
use giftbox\Vue\VueFormulaireGestionnaire;
use giftbox\Vue\VuePanier;
use giftbox\Vue\VuePayement;
use giftbox\Vue\VuePrestation;
use giftbox\Vue\VueRecapitulatif;
use giftbox\models\Prestation;
use giftbox\models\Categorie;
use giftbox\models\Utilisateur;
use giftbox\models\Coffret;
use giftbox\models\ContenuCoffret;
use giftbox\models\Resultat;
use giftbox\models\Note;
use util\Authentication;

class Controleur{

    protected $httpRequest;

    public function __construct($http){
        $this->httpRequest = $http;
    }

    public function afficherAccueil(){
        $v = new VueAccueil($this->httpRequest);
        echo $v->afficher($this->afficherMieuxNoter());
    }

    public function afficherPrestation($id){
        $lp = Prestation::where('id',"=","$id")->first();
        $vue = new VuePrestation($this->httpRequest,$lp->nom);
        echo $vue->afficher($lp);
    }

    public function afficherPanier(){
        $vue = new VuePanier($this->httpRequest);
        echo $vue->afficher()."</br>";
    }

    public function afficherRecap(){
        $vue = new VueRecapitulatif($this->httpRequest);
        echo $vue->afficher()."</br>";
    }

    public function afficherPaye(){
        $vue = new VuePayement($this->httpRequest);
        echo $vue->afficher()."</br>";
    }

    public function afficherInscription($att){
        $vue = new VueInscription($this->httpRequest);
        echo $vue->afficherInscription($att);
    }

    public function afficherApresInscription(){
        $vue=new VueApresInscription($this->httpRequest);
        echo $vue->afficherApresInscription();
    }

    public function ajouterPrestationCoffret($idprest){
        if (isset($idprest)) {
            if (isset($_SESSION['panier'])){
                $tab=$_SESSION['panier'];
                if(isset($tab[$idprest])){
                    $tab[$idprest]=$tab[$idprest]+1;
                }
            else{
                $tab[$idprest]=1;
            }
            $_SESSION['panier']=$tab;
        }
        else{
            $_SESSION['panier'][$idprest]=1;
        }
    }
        echo $this->afficherCatalogue();
    }

    public function supprimerArticlePanier($idprest){
        unset($_SESSION['panier'][$idprest]);
        echo $this->afficherPanier();
    }

    public function __get($attName) {
        if(property_exists($this, $attName))
            return $this->$attName;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

    public function __set($attName, $value) {
        if(property_exists($this, $attName))
            $this->$attName = $value;
        else throw new \Exception("Erreur : attribut ".$attName." inexistant.", 1);
    }

    public function afficherFormulairePrestation(){
        if (isset($_SESSION['profil'])) {
            if ($_SESSION['profil']['auth_level']==100) {
                $vue = new VueFormulaireGestionnaire($this->httpRequest);
                echo $vue->afficher("","","");
            }
            else{
                $c = new ControleurCatalogue($this->httpRequest);
                echo $c->afficherCatalogue(0);
            }
        }
        else{
            echo $this->afficherAccueil();
        }
    }
    public function ajoutPrestationBase(){
        $vue = new VueFormulaireGestionnaire($this->httpRequest);
        if (isset($_SESSION['profil'])) {
            if ($_SESSION['profil']['auth_level']==100) {
        if($_POST['nom_prest']!=""&&$_POST['desc_prest']!=""&&$_POST['idcateg_prest']!=""&&$_POST['img_prest']!=""&&$_POST['prix_prest']!=""){
            $u=new Prestation();
            $u->nom=$_POST['nom_prest'];
            $u->descr=$_POST['desc_prest'];
            $u->cat_id=$_POST['idcateg_prest'];
            $u->img=$_POST['img_prest'];
            $u->prix=$_POST['prix_prest'];
            $u->active=null;
            $c=Prestation::where('nom','=',$_POST['nom_prest'])->first();
            if (!isset($c)) {
                $cc=Categorie::where('id','=',$_POST['idcateg_prest'])->first();
                if (isset($cc)) {
                    $u->save();
                    echo $vue->afficherAjout("Prestation $u->nom ajoutée");
                }
                else{
                    echo $vue->afficherAjout("Catégorie inexistante (id:$u->cat_id)");
                }
            }
            else{
                echo $vue->afficherAjout("Prestation $u->nom déjà existante");
            }   
        }
        else{
            echo $vue->afficherAjout("Un des champs n'a pas été remplit");
        }
    }
    else{
        echo $vue->afficherAjout("Droits insuffisants");
    }
}
    }

    public function supprimerPrestationBase(){
        $p=new Prestation();
        $vue = new VueFormulaireGestionnaire($this->httpRequest);
        if (isset($_SESSION['profil'])) {
            if ($_SESSION['profil']['auth_level']==100) {
                if ($_POST['nom_suppr']!="") {
                    $p=Prestation::where('nom','=',$_POST['nom_suppr'])->first();
                    if (isset($p)) {
                        if (isset($_SESSION['panier'][$p->id])) {
                            unset($_SESSION['panier'][$p->id]);
                        }
                        echo $vue->afficherSuppr("Suppression de la prestation nommée $p->nom");
                        $p->delete();
                    }
                    else{
                        echo $vue->afficherSuppr("Prestation inéxistante");   
                    }
                }
                else{
                    echo $vue->afficherSuppr("Champs non remplit");
                }
            }
            else{
                echo $vue->afficherSuppr("Droits insuffisants");
            }
        }

    }

    public function modifierPrestationBase(){
        $vue = new VueFormulaireGestionnaire($this->httpRequest);
        $p=new Prestation();
        if (isset($_SESSION['profil'])) {
            if ($_SESSION['profil']['auth_level']==100) {
        if ($_POST['nom_modif']!="") {
            $p=Prestation::where('nom','=',$_POST['nom_modif'])->first();
            if (isset($p)) {
                if (isset($_SESSION['panier'][$p->id])) {
                    unset($_SESSION['panier'][$p->id]);
                }
                if ($p->active==null) {
                    $p->active=1;
                    $p->save();
                    echo $vue->afficherModif("Désactivation de la prestation nommée $p->nom");
                }
                else{
                    $p->active=null;
                    $p->save();
                    echo $vue->afficherModif("Activation de la prestation nommée $p->nom");    
                }
            }
            else{
                echo $vue->afficherModif("Prestation inexistante");   
            }
        }
        else{
            echo $vue->afficherModif("Champs nom non remplit");
        }
    }
    else{
        echo $vue->afficherModif("Droits insuffisants");
    }
}
}
    public function inscription(){
            $u=$_POST['email'];
            $p1=$_POST['password'];
            $p2=$_POST['password-repeat'];
            $this->afficherInscription(Authentication::createUser($u,$p1,$p2));
    }

    public function connexion(){
        $u=$_POST['email'];
        $p=$_POST['password'];
        $s="";
        if(Authentication::authenticate($u,$p)){
            Authentication::loadProfile($u);
            /*
            $c = new ControleurCatalogue($this->httpRequest);
            echo $c->afficherCatal(0);
            */
            echo $this->afficherAccueil();
        }
        else{
            if ($u==''||$p=='') {
                $s="Un des champs n'a pas été remplit";
            }
            else{
                $ps=Utilisateur::where("pseudonyme","=",$u)->first();
                if (isset($ps)) {
                    $s="Mot de passe incorrect";
                }
                else{
                    $s="Pseudonyme incorrect";
                }
            }
            echo $this->afficherConnexion($s);
        }
    }

    public function deconnexion(){
        Authentication::disconnect();
        $c = new Controleur($this->httpRequest);
        echo $c->afficherConnexion("");
    }

    public function afficherConnexion($att){
        $vue=new VueConnexion();
        echo $vue->afficherConnexion($att);
    }

    public function verification_droits($auth_level){
        return Authentication::checkAccessRights($auth_level);
    }

    public function afficherCoffret($idCoff,$tok)
    {
        $vue = new VueCoffretCadeau($this->httpRequest,$idCoff,$tok);
        // 1- Faire un select id where = token
        $idTmp = Coffret::select('idCoffret')
            ->where('token', 'like', "$tok")->first();
        if (!$idTmp == null) {
            if (($idCoff == $idTmp->idCoffret)==1){
                echo $vue->afficher($idCoff);
            } else {
                // Le token n'existe pas
                echo $vue->afficher(0);
            }
        }else {
            // Le token ne correspond pas à l'id
            echo $vue->afficher(0);
        }
    }

    public function afficherUrl(){
        $vue = new VueUrl($this->httpRequest);
        echo $vue->afficher($this->genererTokenUrl());
    }

    public function genererTokenUrl()
    {
        $coffretInser = new Coffret();
        $coffretInser->prixTotal = 0;
        $coffretInser->restePaye = 0;
        $char = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-_';
        $token = str_shuffle($char);
        $token = substr($token, 0, rand(15, 30));
        $coffretInser->token = $token;
        $coffretInser->save();
        $prix = $this->retournerPrix();
        $coffretInser->prixTotal = $prix;
        $this->remplirTableContenu($coffretInser->idCoffret);
        return "localhost/myGiftBox/index.php/cadeau-coffret/$coffretInser->idCoffret?token=$token";
    }

    public function remplirTableContenu($id){
        if(isset($_SESSION['panier'])){
            $tab = $_SESSION['panier'];
            foreach($tab as $k=>$value){
                $contenu = new ContenuCoffret();
                $contenu->qte=$value;
                $contenu->idPrestation = $k;
                $contenu->idCoffret = $id;
                $contenu->save();
            }
        }
    }

    public function retournerPrix(){
        $prix = 0;
        if(isset($_SESSION['panier'])){
            $tab = $_SESSION['panier'];
            foreach($tab as $k=>$value){
                $res =Prestation::where('id','=',"$k")->first();
                $prix += ($res->prix*$value);
            }
        }
        return $prix;
    }

    public function mettreNote($idpres, $note){
        // Ajout de la note dans Note
        $lp = Prestation::where('id',"=","$idpres")->first();
        $idCategorie = $lp->cat_id;
        $not= new Note();
        $not->idprest=$idpres;
        $not->idcateg=$idCategorie;
        $not->note=$note;
        $not->save();
        // Ajout de la note ds Resultat
        $rst = Resultat::where("idPrestation","like","$idpres")->first();
        $rst->total = $rst->total+$note;
        $rst->nbNotes = $rst->nbNotes +1;
        $rst->save();
        // Redirection
        $ct = new ControleurCatalogue($this->httpRequest);
        $ct->afficherCatalogue(0,"alpha");
    }

    public function afficherMieuxNoter(){
        $tab = array();
        $listeCateg = Categorie::select("id")->get();
        foreach($listeCateg as $k=>$v){
            $listeNotes = Resultat::where("idcateg","=","$v->id")->get();
            $note = 0;
            $id = 0;
            foreach($listeNotes as $l=>$w){
                $tot = $w->total;
                $nbNo = $w->nbNotes;
                if( ($tot/$nbNo) > $note) {
                    $note = $tot/$nbNo;
                    $id = $w->idPrestation;
                }
            }
            $tab[]=$id;
        }
        return $tab;
    }

    public function afficherPrestOfferte($t,$idc,$idp){
        $listeAff = ContenuCoffret::select('idPrestation')->where("idCoffret","like","$idc")->get();
        $tab=array();
        foreach($listeAff as $k=>$v){
            $tab[]=$v->idPrestation;
        }
        $idSuivant = array_search($idp,$tab);
        if($idSuivant == (count($tab)-1)){
            $idSuivant = $tab[0];
        }else{
            $idSuivant = $tab[$idSuivant+1];
        }
        $v = new VuePrestationOfferte($this->httpRequest,$t,$idc,$idp);
        echo $v->afficher($idSuivant);
    }
}