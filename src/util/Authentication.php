<?php
namespace util;

use giftbox\models\Utilisateur;
use giftbox\models\Role;

class Authentication{
	public static function createUser($userName, $passWord1, $passWord2){
		if ($userName==""||$passWord1==""||$passWord2=="") {
			return "Un des champs n'a pas été remplit";
		}
		if ($passWord1!=$passWord2) {
			return "Les deux mots de passe ne correspondent pas";
		}
		$g=utilisateur::where('pseudonyme','=',$userName)->first();
		if (isset($g)){
			return "pseudonyme déjà existant";
		}
		else{
			if (strlen($passWord1)<7) {
				return "Mot de passe trop court";
			}
			else{
				$g=new Utilisateur();
				$g->pseudonyme=$userName;
				$g->password=password_hash($passWord1, PASSWORD_DEFAULT,['cost'=>12]);
				$g->roleid=2;
				$g->save();
				return "Gestionnaire $userName ajouté";
			}
		}
	}
	public static function authenticate($userName, $passWord){
		$u=utilisateur::where('pseudonyme','=',$userName)->first();
		if (isset($u)) {
			if (password_verify($passWord,$u->password)) {
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	public static function loadProfile($userName){
		$u=utilisateur::where('pseudonyme','=',$userName)->first();
		$r=role::where('roleid','=',$u->roleid)->first();
		$array = array('uid'=>$u->uid,
					   'pseudonyme'=>$u->pseudonyme,
					   'roleid'=>$r->roleid,
					   'auth_level'=>$r->auth_level);
		unset($_SESSION['profil']);
		$_SESSION['profil']=$array;
	}

	public static function checkAccessRights($required){
		if ($_SESSION['profil']['auth_level']>=$required) {
			return true;
		}
		else{
			return false;
		}
	}

	public static function disconnect(){
		if (isset($_SESSION['profil'])) {
			unset($_SESSION['profil']);	
		}
	}
}